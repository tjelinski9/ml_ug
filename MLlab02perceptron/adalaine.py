import numpy as np
import matplotlib.pyplot as plt
import random
import pandas as pd

df = pd.read_csv('https://archive.ics.uci.edu/ml/machine-learning-databases/iris/iris.data', header=None)
# print(df.tail())
arr_fromDf = df.values
data = []
for line in arr_fromDf[:]:
    temp1, temp2, temp3, temp4, tempName = line[:]
    if tempName == 'Iris-setosa' or tempName == 'Iris-versicolor':
        data.append([float(temp1), float(temp2), tempName])

params = [random.uniform(-1.0, 1.0), random.uniform(-1.0, 1.0), random.uniform(-1.0, 1.0)]  # weights
# params = [.0, .0, .0]

print(params)

for d in data:
    if d[2] == 'Iris-setosa':
        color = 'r+'
    else:
        color = 'bx'
    plt.plot(d[0], d[1], color)

n = 1  # iterations
eta = 0.0005
paramsList = []
x = np.linspace(4.0, 7.0, 2)
didNotFind = True
loss = 0
while didNotFind and n <= 100:
    startParams = params[:]
    startLoss = loss
    deltaLoss = 0
    errorSum = [0, 0, 0]
    deltaParams = [.0, .0, .0]
    # didNotFind = False

    for d in data:
        dot = np.dot(startParams, [1, d[0], d[1]])

        if d[2] == 'Iris-setosa':
            factor1 = 1
        else:
            factor1 = -1

        error = (factor1 - dot)
        # print(error)

        errorSum[0] += error
        errorSum[1] += error * d[0]
        errorSum[2] += error * d[1]

        # if error:
        # deltaParams[0] += eta * error * (factor1)
        # deltaParams[1] += eta * d[0] * error * (factor1)
        # deltaParams[2] += eta * d[1] * error * (factor1)
        print(errorSum)
        # else:
        #     didNotFind = False

    # loss = deltaLoss / 2
    # print(loss)

    # for d in data:
    params[0] += errorSum[0] * eta
    params[1] += errorSum[1] * eta
    params[2] += errorSum[2] * eta

    # for l in range(len(params)):
    #     params[l] += deltaParams[l]

    print(n, params)
    n += 1

if not didNotFind:
    print("line found:", params)
    print("iterations:", n)

plt.plot(x, (params[1] * x + params[0]) / ((-1) * params[2]), color=(1, 0, 0, 1))

plt.grid()
plt.show()

import padasip as pa

# # creation of data
# N = 500
# x = np.random.normal(0, 1, (N, 4)) # input matrix
# v = np.random.normal(0, 0.1, N) # noise
# d = 2*x[:,0] + 0.1*x[:,1] - 4*x[:,2] + 0.5*x[:,3] + v # target
#
# # identification
# f = pa.filters.FilterLMS(n=4, mu=0.1, w="random")
# y, e, w = f.run(d, x)
#
# # show results
# plt.figure(figsize=(15,9))
# plt.subplot(211);plt.title("Adaptation");plt.xlabel("samples - k")
# plt.plot(d,"b", label="d - target")
# plt.plot(y,"g", label="y - output");plt.legend()
# plt.subplot(212);plt.title("Filter error");plt.xlabel("samples - k")
# plt.plot(10*np.log10(e**2),"r", label="e - error [dB]");plt.legend()
# plt.tight_layout()
# plt.show()
