import numpy as np
import matplotlib.pyplot as plt
import random

data = []
file = open('iris.data', 'r')

for line in file:
    temp1, temp2, temp3, temp4, tempName = line[0:-1].split(sep=',')
    if tempName == 'Iris-setosa' or tempName == 'Iris-versicolor':
        data.append([float(temp1), float(temp2), tempName])

params = [random.uniform(-1.0, 1.0), random.uniform(-1.0, 1.0), random.uniform(-1.0, 1.0)]  # weights

# x = np.linspace(4.0, 7.0, 2)
# plt.plot(x, (params[1] * x + params[0]) / ((-1) * params[2]), color=(0, 1, 0, 1))

print(params)

for d in data:
    if d[2] == 'Iris-setosa':
        color = 'r+'
    else:
        color = 'bx'
    plt.plot(d[0], d[1], color)


n = 1  # iterations count
eta = .1
paramsList = []
x = np.linspace(4.0, 7.0, 2)
didNotFind = True
while didNotFind and n < 1000:
    startParams = params[:]
    misclassified = 0
    didNotFind = False

    for d in data:
        dot = np.dot(params, [1, d[0], d[1]])

        if dot > 0:
            factor1 = 1
        else:
            factor1 = -1

        if d[2] == 'Iris-setosa':
            factor2 = 1
        else:
            factor2 = -1

        if factor1 != factor2:
            didNotFind = True
            misclassified += 1

        params[0] += eta * (factor2 - factor1)
        params[1] += eta * d[0] * (factor2 - factor1)
        params[2] += eta * d[1] * (factor2 - factor1)

    n += 1


if not didNotFind:
    print("line found:", params)
    print("iterations:", n)

plt.plot(x, (params[1] * x + params[0]) / ((-1) * params[2]), color=(1, 0, 0, 1))

plt.grid()
plt.show()
