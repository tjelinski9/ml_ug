import nltk

nltk.download('brown')
from nltk.corpus import brown
from nltk.tag import UnigramTagger, BigramTagger, TrigramTagger

print(brown.categories())
sentences = brown.tagged_sents(categories='hobbies')
print('sample size:', len(sentences), '\n')
# for i in range(15):
#     print(len(sentences[i]))

division = int(0.80*len(sentences))
train = sentences[:division]
test = sentences[division:]

unigram = UnigramTagger(train)
bigram = BigramTagger(train, backoff=unigram)
trigram = TrigramTagger(train, backoff=bigram)

print('category: hobbies'
      '\ntrain size:', division, '\ntest size:', len(sentences)-division,
      '\n\nunigram:', unigram.evaluate(test),
      '\nbigram:', bigram.evaluate(test),
      '\ntrigram:', trigram.evaluate(test))
