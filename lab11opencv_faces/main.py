import cv2 as cv
import imutils
import numpy
import numpy as np

size = (1280, 720)

face_cascade = cv.CascadeClassifier(cv.data.haarcascades + 'haarcascade_frontalface_default.xml')
smile_cascade = cv.CascadeClassifier(cv.data.haarcascades + 'haarcascade_smile.xml')
eye_cascade = cv.CascadeClassifier(cv.data.haarcascades + 'haarcascade_eye.xml')
scaling_factor = .5

frame = cv.imread("1.jpg")
frame = cv.resize(frame, None, fx=scaling_factor, fy=scaling_factor, interpolation=cv.INTER_AREA)
face_rects = face_cascade.detectMultiScale(frame, scaleFactor=1.3, minNeighbors=5)

for(x, y, w, h) in face_rects:
	cv.rectangle(frame, (x, y), (x+w, y+h), (255, 200, 200), 4)

cv.imshow("1.jpg", frame)
cv.waitKey(0)
cv.destroyAllWindows()


frame = cv.imread("2.jpg")
scaling_factor = 0.32
frame = cv.resize(frame, None, fx=scaling_factor, fy=scaling_factor, interpolation=cv.INTER_AREA)
gray_filter = cv.cvtColor(frame, cv.COLOR_BGR2GRAY)
face_rects = face_cascade.detectMultiScale(gray_filter, scaleFactor=1.3, minNeighbors=4)

for(x, y, w, h) in face_rects:
	cv.rectangle(frame, (x, y), (x+w, y+h), (200, 255, 200), 8)
	roi_gray = gray_filter[y:y+h, x:x+w]
	roi_color = frame[y:y+h, x:x+w]
	smile = smile_cascade.detectMultiScale(roi_gray)
	eye = eye_cascade.detectMultiScale(roi_gray)
	for (sx, sy, sw, sh) in smile:
		cv.rectangle(roi_color, (sx, sy), (sx + sw, sy + sh), (0, 255, 0), 1)
	for (ex, ey, ew, eh) in eye:
		cv.rectangle(roi_color, (ex, ey), (ex + ew, ey + eh), (0, 0, 255), 1)


frame = cv.resize(frame, (int(frame.shape[1]/2), int(frame.shape[0]/2)))
cv.imshow("2.jpg", frame)
cv.waitKey(0)
cv.destroyAllWindows()
print('Found', len(face_rects), 'faces')


hog = cv.HOGDescriptor()
hog.setSVMDetector(cv.HOGDescriptor_getDefaultPeopleDetector())
cv.startWindowThread()
cap = cv.VideoCapture('3.mp4')

while True:
	ret, frame = cap.read()

	frame = cv.resize(frame, size)
	# gray_filter = cv.cvtColor(frame, cv.COLOR_BGR2GRAY)
	# boxes, weights = hog.detectMultiScale(frame, winStride=(7, 7))
	# boxes = numpy.array([[x, y, x+w, y+h] for (x, y, w, h) in boxes])
	#
	# for (xa, ya, xb, yb) in boxes:
	# 	cv.rectangle(frame, (xa, ya), (xb, yb), (200, 200, 255), 4)
	face_rects = face_cascade.detectMultiScale(frame, scaleFactor=1.3, minNeighbors=5)

	for(x, y, w, h) in face_rects:
		cv.rectangle(frame, (x, y), (x+w, y+h), (255, 200, 200), 4)

	cv.imshow('3.mp4', frame)
	if cv.waitKey(1) & 0XFF == ord('q'):
		break

cap.release()
cv.destroyAllWindows()


hog = cv.HOGDescriptor()
hog.setSVMDetector(cv.HOGDescriptor_getDefaultPeopleDetector())
cv.startWindowThread()
cap = cv.VideoCapture('4.mp4')

iterator = 0
size = (960, 540)
while True:
	ret, frame = cap.read()

	frame = cv.resize(frame, size)
	gray_filter = cv.cvtColor(frame, cv.COLOR_RGB2GRAY)
	boxes, weights = hog.detectMultiScale(frame, winStride=(8, 8))
	boxes = numpy.array([[x, y, x+w, y+h] for (x, y, w, h) in boxes])

	for (xa, ya, xb, yb) in boxes:
		cv.rectangle(frame, (xa, ya), (xb, yb), (200, 200, 255), 1)

	iterator += 1
	print('frame:', iterator, 'found', len(boxes), 'people')
	cv.imshow('4.mp4', frame)
	if cv.waitKey(1) & 0XFF == ord('q'):
		break

cap.release()
cv.destroyAllWindows()
