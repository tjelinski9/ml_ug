import random
from sklearn import linear_model, svm
from sklearn.neighbors import KNeighborsClassifier
import pandas as pd
import matplotlib.pyplot as plt
from statistics import mean
import numpy as np

train_set_size = 3133
test_set_size = 1044
n = 4

dat = pd.read_csv('abalone.data', delimiter=',')
list_of_rows = [list(row[1:]) for row in dat.values]
constant_list_of_rows = list_of_rows[:]
# print(list_of_rows)
scores = []  # [way of manipulation][algorithm][number of trial]
for i in range(4):
    scores.append([[], [], []])


def regression(x, y, X, Y):
    model = linear_model.LinearRegression().fit(x, y)
    score = model.score(X, Y)
    return score


def svc(x, y, X, Y):
    model = svm.SVC().fit(x, y)
    score = model.score(X, Y)
    return score


def KNN(x, y, X, Y):
    model = KNeighborsClassifier(n_neighbors=n).fit(x, y)
    score = model.score(X, Y)
    return score


def algorithm_set(l):
    scores[l][0].append(regression(trainX, trainY, testX, testY))
    scores[l][1].append(svc(trainX, trainY, testX, testY))
    scores[l][2].append(KNN(trainX, trainY, testX, testY))
    # print('Linear regression prediction score:', regression(trainX, trainY, testX, testY))
    # print('SVC algorithm prediction score:', svc(trainX, trainY, testX, testY))
    # print('KNN k =', n, 'prediction score:', KNN(trainX, trainY, testX, testY))


trainX = [instance[:-1] for instance in list_of_rows[:train_set_size]]
trainY = [instance[-1] for instance in list_of_rows[:train_set_size]]
testX = [instance[:-1] for instance in list_of_rows[train_set_size:]]
testY = [instance[-1] for instance in list_of_rows[train_set_size:]]
print(len(trainX), len(trainY), len(testX), len(testY))
algorithm_set(0)


for i in range(5):
    print(i+1, '/ 5')
    train_set_size = 3133
    data_deletion_columns = []
    how_many_cols = random.randint(4, 5)
    cols = []
    for i in range(how_many_cols):
        cols.append(random.randint(0, 6))
    for row in list_of_rows:
        new_row = []
        for i in range(len(row)):
            if i not in cols:
                new_row.append(row[i])
        data_deletion_columns.append(new_row)
    # print(data_deletion_columns)

    trainX = [instance[:-1] for instance in data_deletion_columns[:train_set_size]]
    trainY = [instance[-1] for instance in data_deletion_columns[:train_set_size]]
    testX = [instance[:-1] for instance in data_deletion_columns[train_set_size:]]
    testY = [instance[-1] for instance in data_deletion_columns[train_set_size:]]
    # print("\ndeleted columns with missing data:")
    algorithm_set(1)

    for row in list_of_rows:
        for i in range(len(row)):
            if random.randint(0, 10) == 0 and i != 7:  # 50% of data
                row[i] = '?'
    # print(len(list_of_rows))

    data_deletion_rows = []
    for row in list_of_rows:
        flag = 1
        for el in row:
            if el == '?':
                flag = 0
        if flag:
            data_deletion_rows.append(row)
    # print(len(data_deletion))

    train_set_size = int(train_set_size / len(list_of_rows) * len(data_deletion_rows))
    trainX = [instance[:-1] for instance in data_deletion_rows[:train_set_size]]
    trainY = [instance[-1] for instance in data_deletion_rows[:train_set_size]]
    testX = [instance[:-1] for instance in data_deletion_rows[train_set_size:]]
    testY = [instance[-1] for instance in data_deletion_rows[train_set_size:]]
    print(len(trainX), len(trainY), len(testX), len(testY))
    # print("\ndeleted rows with missing data:")
    algorithm_set(2)

    data_average_missing = []
    columns_average = []
    for i in range(len(list_of_rows[0])):
        value_sum = 0
        values_count = 0
        for row in list_of_rows:
            if row[i] != '?':
                value_sum += row[i]
                values_count += 1
        columns_average.append(value_sum / values_count)

    for row in list_of_rows:
        new_row = []
        for i in range(len(row)):
            if row[i] == '?':
                row[i] = columns_average[i]
            new_row.append(row[i])
        data_average_missing.append(new_row)
    # print(data_average_missing)
    train_set_size = 3133
    test_set_size = 1044
    trainX = [instance[:-1] for instance in data_average_missing[:train_set_size]]
    trainY = [instance[-1] for instance in data_average_missing[:train_set_size]]
    testX = [instance[:-1] for instance in data_average_missing[train_set_size:]]
    testY = [instance[-1] for instance in data_average_missing[train_set_size:]]
    # print("\naverage of the column:")
    algorithm_set(3)
    list_of_rows = constant_list_of_rows[:]

lin_means = [mean(el[0]) for el in scores[:]]
svm_means = [mean(el[1]) for el in scores[:]]
knn_means = [mean(el[2]) for el in scores[:]]
# print(lin_means, svm_means, knn_means)
labels = ['Regular', 'Deleted columns', 'Deleted rows ', '  Average of the column']

x = np.arange(len(labels))  # the label locations
width = 0.2  # the width of the bars

fig, ax = plt.subplots()
rects2 = ax.bar(x - width, lin_means, width, label='Linear regression')
rects3 = ax.bar(x, svm_means, width, label='SVM')
rects4 = ax.bar(x + width, knn_means, width, label='KNN')

# Add some text for labels, title and custom x-axis tick labels, etc.
ax.set_ylabel('Scores')
ax.set_title('Scores by way of data manipulation')
ax.set_xticks(x)
ax.set_xticklabels(labels)
ax.legend()

ax.bar_label(rects2, padding=3)
ax.bar_label(rects3, padding=3)
ax.bar_label(rects4, padding=3)

ax.plot(x-width, lin_means)
ax.plot(x, svm_means)
ax.plot(x+width, knn_means)
plt.savefig('chart.png')
plt.show()
