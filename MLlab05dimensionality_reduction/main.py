from sklearn import linear_model, svm
from sklearn.preprocessing import StandardScaler
from sklearn.decomposition import PCA, NMF
from sklearn.neighbors import KNeighborsClassifier
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
from sklearn.feature_selection import SelectKBest, chi2, f_classif

train_set_size = 3133
test_set_size = 1044
n = 4

dat = pd.read_csv('abalone.data', delimiter=',')
data = [list(row[1:]) for row in dat.values]
# print(list_of_rows)
scores = []  # [way of manipulation][algorithm]
for i in range(4):  # count of methods
    scores.append([[], [], []])


def regression(x, y, X, Y):
    model = linear_model.LinearRegression().fit(x, y)
    score = model.score(X, Y)
    return score


def svc(x, y, X, Y):
    model = svm.SVC().fit(x, y)
    score = model.score(X, Y)
    return score


def KNN(x, y, X, Y):
    model = KNeighborsClassifier(n_neighbors=n).fit(x, y)
    score = model.score(X, Y)
    return score


def algorithm_set(l):
    scores[l][0] = regression(trainX, trainY, testX, testY)
    scores[l][1] = svc(trainX, trainY, testX, testY)
    scores[l][2] = KNN(trainX, trainY, testX, testY)


trainX = [instance[:-1] for instance in data[:train_set_size]]
trainY = [instance[-1] for instance in data[:train_set_size]]
testX = [instance[:-1] for instance in data[train_set_size:]]
testY = [instance[-1] for instance in data[train_set_size:]]
print(len(trainX), len(trainY), len(testX), len(testY))
algorithm_set(0)

# 1
features = StandardScaler().fit_transform(dat.iloc[:, 1:-1])
pca = PCA(n_components=0.99, whiten=True)
features_pca = pca.fit_transform(features)
print(features.shape[1], '->', features_pca.shape[1])
trainX = [instance[:-1] for instance in features_pca[:train_set_size]]
trainY = [instance[-1] for instance in data[:train_set_size]]
testX = [instance[:-1] for instance in features_pca[train_set_size:]]
testY = [instance[-1] for instance in data[train_set_size:]]
algorithm_set(1)  # pca


# 2
trainX = [instance[:-1] for instance in data[:]]  # features
trainY = [instance[-1] for instance in data[:]]  # target
# testX = [instance[:-1] for instance in data[train_set_size:]]
# testY = [instance[-1] for instance in data[train_set_size:]]
trainX = np.array(trainX)
chi2_selector = SelectKBest(chi2, k=5)
features_kbest = chi2_selector.fit_transform(trainX, trainY)
print(trainX.shape[1], '->', features_kbest.shape[1])
trainX = []
testX = []
for el in features_kbest:
    trainX.append(el)
    testX.append(el)

trainX = [list(instance) for instance in trainX[:train_set_size]]
trainY = [instance[-1] for instance in data[:train_set_size]]
testX = [list(instance) for instance in testX[train_set_size:]]
testY = [instance[-1] for instance in data[train_set_size:]]
algorithm_set(2)  # irrelevant features deletion


# 3
features = dat.iloc[:, 1:-1]
nmf = NMF(n_components=5, random_state=1, max_iter=10000)
features_nmf = nmf.fit_transform(features)
print(len(data[0])-1, '->', features_nmf.shape[1])

trainX = [instance[:] for instance in features_nmf[:train_set_size]]
trainY = [instance[-1] for instance in data[:train_set_size]]
testX = [instance[:] for instance in features_nmf[train_set_size:]]
testY = [instance[-1] for instance in data[train_set_size:]]
algorithm_set(3)  # matrix factorization


lin_means = [el[0] for el in scores[:]]
svg_means = [el[1] for el in scores[:]]
knn_means = [el[2] for el in scores[:]]
# print(lin_means, svg_means, knn_means)

labels = ['Regular', 'PCA', 'Deletion of\nirrelevant features', ' Non-Negative Matrix\nFactorization']
x = np.arange(len(labels))  # the label locations
width = 0.25  # the width of the bars

fig, ax = plt.subplots()
rects2 = ax.bar(x - width, lin_means, width, label='Linear regression')
rects3 = ax.bar(x, svg_means, width, label='SVG')
rects4 = ax.bar(x + width, knn_means, width, label='KNN')

ax.set_ylabel('Scores')
ax.set_title('Scores by way of data dimensionality reduction')
ax.set_xticks(x)
ax.set_xticklabels(labels)
ax.legend(loc='lower right')

ax.bar_label(rects2)
ax.bar_label(rects3)
ax.bar_label(rects4)

# ax.plot(x-width, lin_means)
# ax.plot(x, svg_means)
# ax.plot(x+width, knn_means)
plt.savefig('chart.png')
plt.show()
