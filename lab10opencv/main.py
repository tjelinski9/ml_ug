import cv2 as cv
import imutils
import numpy as np
import random

image1 = cv.imread('drowzee.png')
cv.imwrite('pokemon.png', image1)
cv.imshow('image', image1)
cv.waitKey(0)

cropped = image1[100:230, 300:500]
cv.imshow('image', cropped)
cv.waitKey(0)

resized = cv.resize(image1, (200, 300))
cv.imshow('image', resized)
cv.waitKey(0)

h, w = image1.shape[0:2]
center = (w // 3, h // 3)
M = cv.getRotationMatrix2D(center, -45, 1.0)
rotated = cv.warpAffine(image1, M, (w, h))
cv.imshow("image", rotated)
cv.waitKey(0)

resized2 = imutils.resize(image1, width=400)
blurred = cv.blur(image1, (30, 30))
resized3 = imutils.resize(blurred, width=400)
compiled = np.hstack((resized2, resized3))
cv.imshow("image", compiled)
cv.waitKey(0)

rectangle = image1.copy()
rectangle = cv.rectangle(rectangle, (320, 40), (600, 300), (200, 255, 200), 8)
cv.imshow("image", rectangle)
cv.waitKey(0)

line = image1.copy()
line = cv.line(line, (600, 20), (320, 320), (255, 200, 200), 8)
cv.imshow("image", line)
cv.waitKey(0)

line2 = image1.copy()
points_list = [random.randint(1, 10)*60 for i in range(20)]
points_list2 = [[random.choice(points_list), random.choice(points_list)] for j in range(len(points_list)//2)]
points = np.array([[points_list2]])
line2 = cv.polylines(line2, np.int32(points), 1, (200, 200, 255), 8)
cv.imshow("image", line2)
cv.waitKey(0)

circle = image1.copy()
circle = cv.circle(circle, (430, 320), 100, (200, 255, 200), 8)
cv.imshow("image", circle)
cv.waitKey(0)

text = cv.putText(image1, 'DROWZEE', (150, 300), cv.FONT_HERSHEY_DUPLEX, 4, (255, 200, 200), 15, cv.LINE_4)
cv.imshow("image", text)
cv.waitKey(0)
