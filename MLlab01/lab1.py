import statistics
import matplotlib.pyplot as plt
import numpy as np
import random
import seaborn as sns

years = []
salaries = []

with open('data.txt', 'r') as f:

    for line in f:
        years.append(int(line.split()[0]))
        salaries.append(float(line.split()[1]))

# print(years)
# print(salaries)

print("wartość oczekiwana:", end =" ")
print(round(sum(salaries) / len(salaries), 2), end =" zł\n")
print("średnia pensja:", end =" ")
print(round(statistics.mean(salaries), 2), end =" zł\n")
print("mediana pensji:", end =" ")
print(statistics.median(salaries), end =" zł\n")
print("moda pensji:", end =" ")
print( "brak mody") #statistics.mode(salaries), end =" zł\n")
print("posortowane pensje:", end =" ")
print(sorted(salaries))


plt.bar(years, salaries)
plt.xlabel('Rok')
plt.ylabel('Pensja')
plt.show()
# list = [1, 2, 3, 4]
# vals = list[-2:-1]
# print(list[:])

# for i in range(1):
#     print(1)
# else:
#     print(3)
# tup = (2, 3)
# fun = 3
# def fun(fun):
#     return fun
# print = 3
# y = fun(2)
